<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name></name>
    <message>
        <location filename="../code/A1_unzip.py" line="46"/>
        <source>Succesfully unzip {os.path.basename(name)}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../code/A1_unzip.py" line="49"/>
        <source>Fail to unzip {os.path.basename(name)}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../code/A1_unzip.py" line="64"/>
        <source>Unzipping...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../code/A2_raw_data_process.py" line="35"/>
        <source>Computing params by municipio...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../code/A2_raw_data_process.py" line="121"/>
        <source>Empty shapefile for {state} {municipio} {param}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../code/A2_raw_data_process.py" line="124"/>
        <source>Invalid shapefile for {state} {municipio} {param}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SicarDataAnalysis</name>
    <message>
        <location filename="../sicar_data_analysis.py" line="238"/>
        <source>&amp;SICAR data analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sicar_data_analysis.py" line="208"/>
        <source>SICAR plugin configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sicar_data_analysis.py" line="216"/>
        <source>Unzip raw data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sicar_data_analysis.py" line="225"/>
        <source>Preprocessing data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>sicar_data_analysisDialogBase</name>
    <message>
        <location filename="../sicar_data_analysis_config_base.ui" line="14"/>
        <source>SICAR data analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sicar_data_analysis_config_base.ui" line="116"/>
        <source>Working directory path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sicar_data_analysis_config_base.ui" line="129"/>
        <source>EPSG code for reprojection </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sicar_data_analysis_config_base.ui" line="142"/>
        <source>Max overlap percent for Area Imovel exclusion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sicar_data_analysis_config_base.ui" line="155"/>
        <source>Min and max percent value to affect a Reserva Legal to an Area Imovel&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sicar_data_analysis_config_base.ui" line="168"/>
        <source>betwenn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sicar_data_analysis_config_base.ui" line="184"/>
        <source>and</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sicar_data_analysis_config_base.ui" line="200"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
