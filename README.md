# SICAR data analysis


## Description
QGIS3.X plugin to help computation of compensation beetwen area imovels and reserva legal in Brazil. 


## Installation
Within QGIS3.

## Support
https://gitlab.huma-num.fr/sthery/sicar_data_analysis/-/issues

## Roadmap
Undergoing

## Authors and acknowledgment
Coding:

   - Sylvain Théry - UMR 5281 ART-Dev - 2022/2023


Algorithm:

- Pierre Gautereau - UMR 8586 Prodig - Paris, France
- Ludivine Eloy - UMR 5281 ART-Dev - Montpellier, France
- Sylvain Théry - UMR 5281 ART-Dev - Montpellier, France
- Eve-Anne Bühler - NUCLAMB - Rio de Janeiro, Brazil

## License


This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License


## Project status
Still experimental