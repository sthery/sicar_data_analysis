#!/usr/bin/env python3
# coding: utf-8
"""
Coding:
    Sylvain Théry - UMR 5281 ART-Dev - 2022/2023
    CC-by-SA-NC V4
Algorithm:
    Pierre Gautereau - UMR 8586 Prodig - Paris, France
    Sylvain Théry - UMR 5281 ART-Dev - Montpellier, France
    Ludivine Eloy - UMR 5281 ART-Dev - Montpellier, France
    Eve-Anne Bühler - Núcleo de Estudos Geoambientais (NUCLAMB) - Rio de Janeiro, Brazil
"""

import logging
import datetime
import shutil
import time
import glob
from zipfile import ZipFile
import os

from qgis.core import QgsMessageLog, Qgis
from qgis.utils import iface
from .Z_miscellaneous import create_MessageBar, set_MessageBarProgress


def unpack_zip(self=None, zipfile='', path_from_local='', plugin=True, delete=False):
    """
    Extract recursively unzip files in zipfile
    Taken and adapted from https://gist.github.com/JamesPHoughton/0f4f269e93a2b85958d8
    :param zipfile: str: basename of the file to extract
    :param path_from_local:
    :param delete: bool: delete zip file after extracting
    :return: str:
    """
    filepath = path_from_local + zipfile
    extract_path = f"{filepath.strip('.zip')}/".replace('0_zipped_data', '1_raw_data')
    parent_archive = ZipFile(filepath)
    parent_archive.extractall(extract_path)
    namelist = parent_archive.namelist()
    parent_archive.close()
    if delete:
        os.remove(filepath)
    for name in namelist:
        try:
            if name[-4:] == '.zip':
                unpack_zip(zipfile=name, path_from_local=extract_path, plugin=plugin, delete=True)
                #TODO rajouter municip state
                message = self.tr(f"Succesfully unzip {os.path.basename(name)}")
                QgsMessageLog.logMessage(self.tr(message), self.plugin_name, level=Qgis.Info)
        except:
            message = self.tr(f"Fail to unzip {os.path.basename(name)}")
            QgsMessageLog.logMessage(self.tr(message), self.plugin_name, level=Qgis.Critical)

    return extract_path

    # # you can just call this with filename set to the relative path and file.
    # path = unpack_zip(filename)

def A1_unzip(self, delete=False):
    #remove dest path if exists
    try:
        shutil.rmtree(f"{self.root_path}/1_raw_data/")
    except:
        pass
    source_path= f"{self.root_path}/0_zipped_data/"

    # prepare values for progressbar
    i = 0
    i_tot = 0
    for working_dir in glob.glob(f"{source_path}/*/"):
        i_tot+=len(glob.glob(working_dir + '*.zip'))

    message = self.tr("Unzipping...")
    create_MessageBar(self, message)
    self.progressBar.setMaximum(i_tot)
    iface.messageBar().pushWidget(self.progressMessageBar, Qgis.Info)
    for working_dir in glob.glob(f"{source_path}/*/"):
        # TOOO ajouter log
        for zipfile in glob.glob(working_dir + '*.zip'):
            municipio = os.path.basename(zipfile)[:-4]
            state = os.path.basename(working_dir[:-1])
            unpack_zip(self, zipfile, delete=delete)
            percent = int(i / float(i_tot) * 100)
            iface.statusBarIface().showMessage(f"Proceeding {state} {municipio} ({percent}%)")
            i+=1
            set_MessageBarProgress(self, i, municipio)

    iface.statusBarIface().clearMessage()
    iface.messageBar().clearWidgets()


