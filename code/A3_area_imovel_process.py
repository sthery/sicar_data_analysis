#!/usr/bin/env python3
# coding: utf-8
"""
Coding:
    Sylvain Théry - UMR 5281 ART-Dev - 2022/2023
    CC-by-SA-NC V4
Algorithm:
    Pierre Gautereau - UMR 8586 Prodig - Paris, France
    Sylvain Théry - UMR 5281 ART-Dev - Montpellier, France
    Ludivine Eloy - UMR 5281 ART-Dev - Montpellier, France
    Eve-Anne Bühler - Núcleo de Estudos Geoambientais (NUCLAMB) - Rio de Janeiro, Brazil
"""

import glob
import logging
import os
import shutil
from datetime import datetime
import sys
from PyQt5.QtCore import QVariant
from qgis.core import QgsSpatialIndex, QgsFeatureRequest, QgsProject, QgsCoordinateTransformContext, \
    QgsCoordinateReferenceSystem, QgsFields, QgsProcessingFeatureSourceDefinition, \
    QgsAbstractFeatureIteratorFromSourceQgsVectorLayerFeatureSourceBase, Qgis, \
    QgsField, QgsVectorLayer, QgsApplication, QgsVectorFileWriter, QgsFeature, QgsWkbTypes, edit
from qgis.utils import iface
from collections import defaultdict
import time

from qgis.utils import iface

from .Z_miscellaneous import move_to_group, create_MessageBar, set_MessageBarProgress, create_empty_gpkg_layer

import processing


def A1_create_dict_of_param_by_municipio(self, path):
    """

    """
    d = defaultdict(list)
    for filename in os.listdir(path):
        if filename.endswith('.gpkg'):
            left, right = filename.split('.')[0].rsplit("_",1)
            d[right].append(left)

    # trier les listes de valeurs
    for key in d:
        d[key] = sorted(d[key])
    return d


def A_3_immovel_process(self):
    # TODO remove when end debug ?
    # try:
    #     shutil.rmtree(f"{self.root_path}/3_imovel_process/")
    # except:
    #     pass

    print("A_3_immovel_process start")
    dest_path = f"{self.root_path}/3_imovel_process/"
    os.makedirs(f"{dest_path}", exist_ok=True)

    A3_1_imovel_categorization(self, dest_path)
    A3_2_a_param_concatenation(self, dest_path)
    A3_2_b_IDF_attribution(self, dest_path)
    print("A_3_immovel_process end")
    iface.statusBarIface().clearMessage()
    iface.messageBar().clearWidgets()


def A3_1_imovel_categorization(self, dest_path):

    i_tot = 0

    for working_dir in sorted(glob.glob(f"{self.root_path}/2_cleaned_data/05_geom_changed/*/")):
        for gpkg in sorted(glob.glob(working_dir + "/*.gpkg")):
            filename = os.path.basename(gpkg)
            param, municipio = filename.split('.')[0].rsplit("_", 1)

            if param == 'AREA_IMOVEL':
                i_tot += 1
    message = self.tr("Area imovel classification...")
    create_MessageBar(self, message)
    self.progressBar.setMaximum(i_tot)
    iface.messageBar().pushWidget(self.progressMessageBar, Qgis.Info)
    i = 0

    for working_dir in sorted(glob.glob(f"{self.root_path}/2_cleaned_data/04_single_part_orginal_geom_type/*/")):
        state = os.path.basename(working_dir[:-1])
        for gpkg in sorted(glob.glob(working_dir + "/*.gpkg")):
            filename = os.path.basename(gpkg)
            param, municipio = filename.split('.')[0].rsplit("_", 1)

            if param == 'AREA_IMOVEL':
                set_MessageBarProgress(self, i, municipio)
                i += 1
                AREA_IMOVEL_selected_because_not_contained_any = A3_1_a_add_number_of_polys_contained_in_one_poly(self,
                                    gpkg, state, param, municipio, working_dir, dest_path)


                mem_layer_AI_selected_because_overlap_ok= A3_1_b_add_overlap_values_of_polys_contained_in_one_poly(self,
                                    AREA_IMOVEL_selected_because_not_contained_any, param, municipio, state, dest_path)

                mem_layer_AI_fixed = A3_1_c_fix_geometries(self, mem_layer_AI_selected_because_overlap_ok, param, municipio, state, dest_path)
                #
                mem_layer_RL_points, mem_layer_AI_poly = A3_1_d_count_RL_number(self, mem_layer_AI_fixed, param, municipio,
                                    state, dest_path)

                mem_layer_AI_whith_RL_count_and_num_parts = A3_1_e_join_RL_nb_parts(self, mem_layer_RL_points,
                                                    mem_layer_AI_poly, param, municipio, state, dest_path)

                layer_AI_whith_RL_count_and_num_parts_and_IDF_unique_filename = A3_1_f_join_AI_nb_IDF(self,
                                mem_layer_RL_points, mem_layer_AI_whith_RL_count_and_num_parts, param, municipio, state, dest_path)
                AI_layer_with_full_attributes = A3_1_g_classif(self, layer_AI_whith_RL_count_and_num_parts_and_IDF_unique_filename, param, municipio, state, dest_path)

    iface.messageBar().clearWidgets()
    iface.statusBarIface().clearMessage()
    return AI_layer_with_full_attributes, state, param, municipio


def A3_1_a_add_number_of_polys_contained_in_one_poly(self, input_layer_name, state, param, municipio, working_dir,
                                                     dest_path):

    os.makedirs(dest_path, exist_ok=True)
    print("    Dealing with poly inside poly")

    input_layer = QgsVectorLayer(f'{working_dir}{param}_{municipio}.gpkg', f'{param}_{municipio}', 'ogr')
    options = QgsVectorFileWriter.SaveVectorOptions()
    options.driverName = "GPKG"
    options.layerName = "AREA_IMOVEL_with_count"
    options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteFile
    options.EditionCapability = QgsVectorFileWriter.CanAddNewLayer
    options.fileEncoding = "utf-8"

    # write new gpkg
    dest_file_path = f"{dest_path}/07_select_imovel_contained_in/{state}/{os.path.basename(input_layer.name())}_imovel_poly_contained.gpkg"
    os.makedirs(os.path.dirname(dest_file_path), exist_ok=True)
    new_layer = QgsVectorFileWriter.writeAsVectorFormatV3(input_layer, dest_file_path, QgsCoordinateTransformContext(),
                                                          options)
    poly_layer = QgsVectorLayer(f"{dest_file_path}|layername=AREA_IMOVEL_with_count", "AREA_IMOVEL", "ogr")

    # Create a new field
    fields = poly_layer.fields()
    fields.append(QgsField("contained_count", QVariant.Int, "integer", 10))
    poly_layer.dataProvider().addAttributes([QgsField("contained_count", QVariant.Int)])
    poly_layer.updateFields()

    # Iterate over the polygons and set the default value for the new field
    poly_layer.startEditing()
    for f in poly_layer.getFeatures():
        poly_layer.changeAttributeValue(f.id(), fields.indexFromName("contained_count"), 0)
    poly_layer.commitChanges()

    # Create a spatial index for the layer
    index = QgsSpatialIndex()
    for f in poly_layer.getFeatures():
        index.addFeature(f)

    # Iterate over the polygons and count the number of contained polygons
    poly_layer.startEditing()
    for f in poly_layer.getFeatures():
        geom = f.geometry()
        contained_count = 0
        for id in index.intersects(geom.boundingBox()):
            other_f = next(poly_layer.getFeatures(QgsFeatureRequest(id)))
            if geom.contains(other_f.geometry()):
                contained_count += 1

        contained_count -= 1
        poly_layer.changeAttributeValue(f.id(), fields.indexFromName("contained_count"), contained_count)

    poly_layer.commitChanges()
    crs = poly_layer.crs()

    # Create two memory layers to store feature with only 1 containing poly (itself) and another for the other
    select_layer = QgsVectorLayer("Polygon?crs=" + crs.authid(), "AREA_IMOVEL_selected_because_not_contained_any",
                                  "memory")
    not_select_layer = QgsVectorLayer("Polygon?crs=" + crs.authid(),
                                      "AREA_IMOVEL_not_selected_because_containing_many_others", "memory")

    # Getting input layer fields list
    provider = poly_layer.dataProvider()
    fields = provider.fields()

    # add list fields to memory layers
    select_provider = select_layer.dataProvider()
    select_provider.addAttributes(fields)
    select_layer.updateFields()

    not_select_provider = not_select_layer.dataProvider()
    not_select_provider.addAttributes(fields)
    not_select_layer.updateFields()

    # Filter by the number of complitly contains number of polygons
    request = QgsFeatureRequest().setFilterExpression("contained_count = 0")
    for feat in poly_layer.getFeatures(request):
        select_provider.addFeature(feat)

    request = QgsFeatureRequest().setFilterExpression("contained_count > 0")
    for feat in poly_layer.getFeatures(request):
        not_select_provider.addFeature(feat)

    # Enregistrer les nouvelles couches dans le même fichier .gpkg
    select_layer.commitChanges()
    not_select_layer.commitChanges()

    options = QgsVectorFileWriter.SaveVectorOptions()
    options.driverName = "GPKG"
    options.fileEncoding = "utf-8"
    options.layerName = select_layer.name()
    options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteLayer
    options.EditionCapability = QgsVectorFileWriter.CanAddNewLayer

    QgsVectorFileWriter.writeAsVectorFormatV3(select_layer, dest_file_path, QgsCoordinateTransformContext(), options)

    options.layerName = not_select_layer.name()
    QgsVectorFileWriter.writeAsVectorFormatV3(not_select_layer, dest_file_path, QgsCoordinateTransformContext(),
                                              options)

    list_layer_to_add = [QgsVectorLayer(f"{dest_file_path}|layername=AREA_IMOVEL_with_count",
                                        f"{os.path.basename(dest_file_path[:-5])} — AREA_IMOVEL_with_count"),
                         QgsVectorLayer(f"{dest_file_path}|layername=AREA_IMOVEL_selected_because_not_contained_any",
                                        f"{os.path.basename(dest_file_path[:-5])} — AREA_IMOVEL_selected_because_not_contained_any"),
                         QgsVectorLayer(
                             f"{dest_file_path}|layername=AREA_IMOVEL_not_selected_because_containing_many_others",
                             f"{os.path.basename(dest_file_path[:-5])} — AREA_IMOVEL_not_selected_because_containing_many_others")]
    for layer_to_add in list_layer_to_add:
        QgsProject.instance().addMapLayer(layer_to_add, False)
        dest_group = self.dict_group_layers[f"{state}_3_imovel_process_07_select_imovel_contained_in_{municipio}"][1]
        dest_group.findGroup(municipio).addLayer(layer_to_add)


    return select_layer

def A3_1_b_add_overlap_values_of_polys_contained_in_one_poly(self, input_layer, param, municipio, state, dest_path):
    print("    Dealing with overlap")
    schema = QgsFields()
    schema.append(QgsField("bool_field", QVariant.Bool))
    output_file_name = f'{dest_path}/08_select_overlap/{state}/{param}_{municipio}_overlaping.gpkg'
    create_empty_gpkg_layer(output_file_name, "temp_table", QgsWkbTypes.NoGeometry, '', schema)
    params = {
        'INPUT': input_layer,
        'LAYERS': [input_layer],
        'OUTPUT': f'ogr:dbname=\'{output_file_name}\' table="AREA_IMOVEL_with_overlap_values" (geom)'}

    output_overlap_layer_result = processing.run('native:calculatevectoroverlaps', params)

    output_overlap_layer = QgsVectorLayer(output_overlap_layer_result["OUTPUT"], 'AREA_IMOVEL_with_overlap_values',
                                          'ogr')
    fields = output_overlap_layer.fields()
    fields_name = [field.name() for field in fields]
    output_overlap_layer.startEditing()
    output_overlap_layer.renameAttribute(len(fields_name) - 1, 'overlapPc')
    output_overlap_layer.renameAttribute(len(fields_name) - 2, 'overlapArea')
    output_overlap_layer.updateFields()
    output_overlap_layer.commitChanges()
    params = {'FIELD': 'overlapArea',
              'INPUT': output_overlap_layer,
              'METHOD': 0, 'OPERATOR': 2, 'VALUE': {int(self.max_overlap)}}
    processing.run("qgis:selectbyattribute", params)

    # Create two memory layers to store feature with only 1 containing poly (itself) and another for the other
    crs = output_overlap_layer.crs()

    not_select_layer = QgsVectorLayer("Polygon?crs=" + crs.authid(),
                                      "AREA_IMOVEL_not_selected_because_to_much_overlap", "memory")
    not_select_layer.dataProvider().addAttributes(output_overlap_layer.fields().toList())
    not_select_layer.updateFields()
    not_select_layer.dataProvider().addFeatures(output_overlap_layer.selectedFeatures())
    not_select_layer.commitChanges()

    output_overlap_layer.invertSelection()
    select_layer = QgsVectorLayer("Polygon?crs=" + crs.authid(),
                                  f"AREA_IMOVEL_selected_because_less_than_{self.max_overlap}_pc", "memory")
    select_layer.dataProvider().addAttributes(output_overlap_layer.fields().toList())
    select_layer.updateFields()
    select_layer.dataProvider().addFeatures(output_overlap_layer.selectedFeatures())
    select_layer.commitChanges()

    # Save layers in the same geopackage .gpkg
    options = QgsVectorFileWriter.SaveVectorOptions()
    options.driverName = "GPKG"
    options.fileEncoding = "utf-8"

    options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteLayer
    options.EditionCapability = QgsVectorFileWriter.CanAddNewLayer

    options.layerName = select_layer.name()
    QgsVectorFileWriter.writeAsVectorFormatV3(select_layer, output_file_name, QgsCoordinateTransformContext(), options)

    options.layerName = not_select_layer.name()
    QgsVectorFileWriter.writeAsVectorFormatV3(not_select_layer, output_file_name, QgsCoordinateTransformContext(),
                                              options)

    list_layer_to_add = [QgsVectorLayer(
        f"{output_file_name}|layername=AREA_IMOVEL_with_overlap_values",
        f"{os.path.basename(output_file_name[:-5])} — AREA_IMOVEL_with_overlap_values"),
        QgsVectorLayer(
            f"{output_file_name}|layername=AREA_IMOVEL_selected_because_less_than_{self.max_overlap}_pc",
            f"{os.path.basename(output_file_name[:-5])} — AREA_IMOVEL_selected_because_less_than_{self.max_overlap}_pc"),
        QgsVectorLayer(
            f"{output_file_name}|layername=AREA_IMOVEL_not_selected_because_to_much_overlap",
            f"{os.path.basename(output_file_name[:-5])} — AREA_IMOVEL_not_selected_because_to_much_overlap")]

    for layer_to_add in list_layer_to_add:
        QgsProject.instance().addMapLayer(layer_to_add, False)
        dest_group = self.dict_group_layers[f"{state}_3_imovel_process_08_select_overlap_{municipio}"][1]
        dest_group.findGroup(municipio).addLayer(layer_to_add)


    return select_layer  # meme layer


def A3_1_c_fix_geometries(self, mem_layer_AI_selected_because_overlap_ok, param, municipio, state, dest_path):
    print("    Dealing geometry fixing")
    output_file_name = f"{dest_path}/09_geom_fix_imovels/{state}/{param}_{municipio}_fixed_geom.gpkg"
    os.makedirs(os.path.dirname(output_file_name), exist_ok=True)


    alg_params = {
        "INPUT": mem_layer_AI_selected_because_overlap_ok,
        'METHOD': 0,
        'OUTPUT': "TEMPORARY_OUTPUT"
    }


    fixgeometries = processing.run('native:fixgeometries', alg_params)

    processing.run("native:savefeatures", {
        'INPUT': fixgeometries["OUTPUT"],
        'OUTPUT': output_file_name,
   'LAYER_OPTIONS': "-a_srs EPSG:29101"})

    layer = QgsVectorLayer(output_file_name, f"{municipio}_{param}_fixed_geom")

    QgsProject.instance().addMapLayer(layer, False)
    dest_group = self.dict_group_layers[f"{state}_3_imovel_process_09_geom_fix_imovels_{municipio}"][1]
    dest_group.findGroup(municipio).addLayer(layer)

    return fixgeometries["OUTPUT"]


def A3_1_d_count_RL_number(self, mem_layer_AI, param, municipio, state, dest_path):
    print("    Dealing with RL number count in AI")

    # Load point RL layer into memory for faster computation
    mem_layer_RL_points = QgsVectorLayer(f"{self.root_path}/2_cleaned_data/05_original_part_type_to_single_part_point_with_area/{state}/RESERVA_LEGAL_{municipio}.gpkg")

    # TODO comprendre pourquoi le passage en meme layer ne fonctionne pas
    # # put the RL poinyt layer in memory for faster compute
    # savefeatures_memory = processing.run("native:savefeatures", {
    #     'INPUT': RL_points,
    #     'OUTPUT': "memory:",
    #     'LAYER_OPTIONS': "-a_srs EPSG:29101"})
    #
    # mem_layer_RL_points = savefeatures_memory['OUTPUT']

    output_file_name = f"{dest_path}/10_imovels_rl_count/{state}/{municipio}_{param}_rl_conut.gpkg"
    os.makedirs(os.path.dirname(output_file_name), exist_ok=True)
    RL_num = 'RL_num'
    alg_params = {
        'CLASSFIELD': '',
        'FIELD': RL_num,
        'POINTS': mem_layer_RL_points,
        'POLYGONS': mem_layer_AI,
        'WEIGHT': '',
        'OUTPUT': "TEMPORARY_OUTPUT"
    }

    countpointsinpolygon = processing.run('native:countpointsinpolygon', alg_params)

    # save layer add it to the tree
    processing.run("native:savefeatures", {
        'INPUT': countpointsinpolygon["OUTPUT"],
        'OUTPUT': output_file_name,
        'LAYER_OPTIONS': "-a_srs EPSG:29101"})
    layer = QgsVectorLayer(output_file_name, f"{municipio}_{param}_rl_num")
    QgsProject.instance().addMapLayer(layer, False)
    dest_group = self.dict_group_layers[f"{state}_3_imovel_process_10_imovels_rl_count_{municipio}"][1]
    dest_group.findGroup(municipio).addLayer(layer)

    return mem_layer_RL_points, countpointsinpolygon["OUTPUT"]


def A3_1_e_join_RL_nb_parts(self, mem_layer_RL_points, mem_layer_AI_poly, param, municipio, state, dest_path):
    print("    Dealing with RL number parts in AI")
    output_file_name = f"{dest_path}/11_add_rl_nb_parts/{state}/{municipio}_{param}_rl_nb_parts.gpkg"

    os.makedirs(os.path.dirname(output_file_name), exist_ok=True)

    # fix poly geom
    alg_params = {
        "INPUT": mem_layer_AI_poly,
        'METHOD': 0,
        'OUTPUT': "TEMPORARY_OUTPUT"
    }
    mem_layer_AI_poly_fixed = processing.run('native:fixgeometries', alg_params)
    alg_params = {
        "INPUT": mem_layer_RL_points,
        'METHOD': 0,
        'OUTPUT': "TEMPORARY_OUTPUT"
    }
    mem_layer_RL_points_fixed = processing.run('native:fixgeometries', alg_params)

    joinattributesbylocation = processing.run("native:joinattributesbylocation", {
        'INPUT': mem_layer_AI_poly_fixed['OUTPUT'],
        'PREDICATE': [0],
        'JOIN': mem_layer_RL_points_fixed['OUTPUT'],
        'JOIN_FIELDS': ['nb_parts_RL'], 'METHOD': 0, 'DISCARD_NONMATCHING': False, 'PREFIX': '',
        'OUTPUT': output_file_name})


    layer = QgsVectorLayer(output_file_name, f"{municipio}_{param}_rl_nb_parts")
    QgsProject.instance().addMapLayer(layer, False)
    dest_group = self.dict_group_layers[f"{state}_3_imovel_process_11_add_rl_nb_parts_{municipio}"][1]
    dest_group.findGroup(municipio).addLayer(layer)

    return layer
    #
    # AI_layer = result_d['OUTPUT']
    #
    # AI_layer.selectAll()
    # AI_memory_layer = processing.plugin_configuration("native:saveselectedfeatures", {'INPUT': AI_layer, 'OUTPUT': 'memory:'})['OUTPUT']
    # RL_layer = QgsVectorLayer(points)
    # RL_layer.selectAll()
    # RL_memory_layer = processing.run("native:saveselectedfeatures", {'INPUT': RL_layer, 'OUTPUT': 'memory:'})['OUTPUT']
    # AI_layer_provider = AI_layer.dataProvider()
    # AI_layer_provider.addAttributes(
    #             [QgsField("RL_area_pc_AI_area", QVariant.Double,"double", 6, 3)])
    # AI_layer.updateFields()
    # AI_layer.startEditing()
    # print("    Dealing with RL percent in AI")
    #
    # for feature in AI_layer.getFeatures():
    #     sql_expression = f''' fid = {feature.id()} '''
    #     AI_layer.setSubsetString(sql_expression)
    #     processing.run("native:selectbylocation",
    #                    {'INPUT': RL_memory_layer,
    #                     'PREDICATE': [0],
    #                     'INTERSECT': AI_layer,
    #                     'METHOD': 0})
    #     area_imovel_area = feature['area']
    #     for RL in RL_memory_layer.selectedFeatures():
    #         reserva_legale_area = RL['area']
    #         if reserva_legale_area > (area_imovel_area * (min_percent_AI_RL/100)) \
    #                 and reserva_legale_area < (area_imovel_area * (max_percent_AI_RL/100)):
    #             feature['RL_area_pc_AI_area'] = int(
    #                     reserva_legale_area / area_imovel_area * 10000) / 100
    #             AI_layer.updateFeature(feature)
    #             break
    # AI_layer.commitChanges()
    # processing.run("native:savefeatures", {
    #     'INPUT': AI_layer,
    #     'OUTPUT': output, 'LAYER_NAME': '', 'Dresult_cATASOURCE_OPTIONS': '',
    #     'LAYER_OPTIONS': "-a_srs 'epsg:29101'"})
    #
    # return AI_layer


def A3_1_f_join_AI_nb_IDF(self, mem_layer_RL_points, mem_layer_AI_whith_RL_count_and_num_parts,
                          param, municipio, state, dest_path):
    print("    Dealing with number of different IDF in AI")
    output_file_name = f"{dest_path}/12_AI_nb_IDF/{state}/{municipio}_{param}_rl_nb_idf.gpkg"

    os.makedirs(os.path.dirname(output_file_name), exist_ok=True)
    params ={'INPUT': mem_layer_AI_whith_RL_count_and_num_parts, 'PREDICATE': [1],
              'JOIN': mem_layer_RL_points,
              'JOIN_FIELDS': ['IDF'], 'SUMMARIES': [1],
              'DISCARD_NONMATCHING': False, 'OUTPUT': output_file_name}

    result = processing.run("qgis:joinbylocationsummary", params)

    layer = QgsVectorLayer(output_file_name, f"{municipio}_{param}_rl_nb_idf")
    QgsProject.instance().addMapLayer(layer, False)
    dest_group = self.dict_group_layers[f"{state}_3_imovel_process_12_AI_nb_IDF_{municipio}"][1]
    dest_group.findGroup(municipio).addLayer(layer)

    return output_file_name

def A3_1_g_classif(self, layer_AI_whith_RL_count_and_num_parts_and_IDF_unique_filename, param, municipio, state, dest_path):
    print("    Dealing classif AI into a,b,c,d type")
    output_file_name = f"{dest_path}/13_AI_classif/{state}/{municipio}_{param}_AI_classif.gpkg"
    os.makedirs(os.path.dirname(output_file_name), exist_ok=True)
    shutil.copyfile(layer_AI_whith_RL_count_and_num_parts_and_IDF_unique_filename, output_file_name)
    AI_layer = QgsVectorLayer(output_file_name, f"{municipio}_{param}_AI_classif")

    layer_provider = AI_layer.dataProvider()
    layer_provider.addAttributes([QgsField("AI_type", QVariant.String)])
    # Re-load the fields from the provider
    AI_layer.updateFields()
    AI_layer.startEditing()
    for feature in AI_layer.getFeatures():
        if feature['RL_num'] == 0:
            feature["AI_type"] = 'a'
        elif feature['RL_num'] == 1:
            if feature['nb_parts_RL'] == 1:
                feature["AI_type"] = 'c'
            else:
                feature["AI_type"] = 'b'
        else:
            feature["AI_type"] = 'd'

        AI_layer.updateFeature(feature)
    AI_layer.commitChanges()
    QgsProject.instance().addMapLayer(AI_layer, False)
    dest_group = self.dict_group_layers[f"{state}_3_imovel_process_13_AI_classif_{municipio}"][1]
    dest_group.findGroup(municipio).addLayer(AI_layer)
    return AI_layer


def A3_2_a_param_concatenation(self, dest_path):
    print("    Merging params for IDF attribution")
    # A3_2_a_param_concatenation
    for working_dir in sorted(glob.glob( f"{self.root_path}/2_cleaned_data/05_original_part_type_to_single_part_point_with_area/*/")):
        state = os.path.basename(working_dir[:-1])

        d = A1_create_dict_of_param_by_municipio(self, working_dir)
        i = 0
        i_tot = len(d.keys())
        for key in d.keys():
            i += 1
            print(f'        Merging params for {key} {i}/{i_tot}')
            list_layers = []
            for value in d[key]:
                if value != 'AREA_IMOVEL' and value != 'RESERVA_LEGAL':
                    layer_name = f"{self.root_path}/2_cleaned_data/05_original_part_type_to_single_part_point_with_area/{state}/{value}_{key}.gpkg"
                    list_layers.append(layer_name)
            output = f'{dest_path}/14_merge_params_for_idf_attribution/{state}/PARAMS_{key}.gpkg'
            os.makedirs(os.path.dirname(output), exist_ok=True)
            mergevectorlayers = processing.run("native:mergevectorlayers", {'LAYERS': list_layers,
                                                        'CRS': None,
                                                        'OUTPUT': output})

            layer = QgsVectorLayer(mergevectorlayers['OUTPUT'], f"PARAMS_{key}")
            QgsProject.instance().addMapLayer(layer, False)
            dest_group = self.dict_group_layers[f"{state}_3_imovel_process_14_merge_params_for_idf_attribution_{key}"][1]
            dest_group.findGroup(key).addLayer(layer)

#<QgsVectorLayer: 'Baianópolis_AREA_IMOVEL_AI_classif' (ogr)>, <QgsVectorLayer: 'PARAMS_Angical' (ogr)>, Bahia, VEREDA, Baianópolis, /home/sylvain/Work/ART-Dev/2023/2023_SICAR/3_imovel_process/

def A3_2_b_IDF_attribution(self, dest_path):
    print("/DEBUG")
    for working_dir in sorted(glob.glob(f"{self.root_path}/3_imovel_process/14_merge_params_for_idf_attribution/*/")):
        state = os.path.basename(working_dir[:-1])
        print(state)
        print(working_dir)
        for gpkg in sorted(glob.glob(working_dir + "/*.gpkg")):
            municipio = gpkg.rsplit("_",1)[1][:-5]
            join_layer = f"{gpkg}"
            input_layer =  f"{dest_path}/13_AI_classif/{state}/{municipio}_AREA_IMOVEL_AI_classif.gpkg"
            print(f"input_layer         {input_layer}")
            output_file_name = f"{dest_path}/15_AI_with_idf_attribution/{state}/{municipio}_IDF_attribution.gpkg"

            os.makedirs(os.path.dirname(output_file_name), exist_ok=True)
            processing.run("native:joinattributesbylocation", {
                'INPUT': input_layer,
                'PREDICATE': [1],
                'JOIN': gpkg,
                'JOIN_FIELDS': ['IDF', 'param'], 'METHOD': 1, 'DISCARD_NONMATCHING': False, 'PREFIX': 'param_',
                'OUTPUT': output_file_name})
            output_layer = QgsVectorLayer(output_file_name, f"{municipio}_IDF_attribution")
            output_layer.dataProvider().addAttributes([QgsField("IDF", QVariant.Int),
                                                       QgsField("IDF_from", QVariant.String)])
            output_layer.updateFields()
            output_layer.commitChanges()
            output_layer.startEditing()
            for feature in output_layer.getFeatures():
                if feature["AI_type"] == 'a' or feature["AI_type"] == 'b':
                    feature["IDF"] = feature["param_IDF"]
                    feature["IDF_from"] = feature["param_param"]
                    output_layer.updateFeature(feature)
            output_layer.commitChanges()


            fieldnames_to_delete = ['param_IDF', 'param_param']
            # Enter Edit mode
            with edit(output_layer):
                # Create empty list we will fill with the fieldindexes
                fields_to_delete = []
                # Iterate over the list of fieldnames and get the indexes
                for fieldname_to_delete in fieldnames_to_delete:
                    # Get the field index by its name:
                    fieldindex_to_delete = output_layer.fields().indexFromName(fieldname_to_delete)
                    # You can also check if the field exists
                    if fieldindex_to_delete == -1:
                        # If it does not exist, just skip it and go to the next one. This may prevent a crash or error :)
                        continue
                    # Append the index to the list
                    fields_to_delete.append(fieldindex_to_delete)
                # Delete the fields by their indexes, note that it has to be a list:
                output_layer.dataProvider().deleteAttributes(fields_to_delete)
            # Update the fields, so the changes are recognized:
            output_layer.updateFields()
            output_layer.commitChanges()
            # load layer in its group
            QgsProject.instance().addMapLayer(output_layer, False)
            dest_group = self.dict_group_layers[f"{state}_3_imovel_process_15_AI_with_idf_attribution_{municipio}"][1]
            dest_group.findGroup(municipio).addLayer(output_layer)

