#!/usr/bin/env python3
# coding: utf-8
"""
Coding:
    Sylvain Théry - UMR 5281 ART-Dev - 2022/2023
    CC-by-SA-NC V4
Algorithm:
    Pierre Gautereau - UMR 8586 Prodig - Paris, France
    Sylvain Théry - UMR 5281 ART-Dev - Montpellier, France
    Ludivine Eloy - UMR 5281 ART-Dev - Montpellier, France
    Eve-Anne Bühler - Núcleo de Estudos Geoambientais (NUCLAMB) - Rio de Janeiro, Brazil
"""
import glob
import os
from time import time, strftime, gmtime

from PyQt5.QtWidgets import QLabel
from qgis.PyQt.QtWidgets import QProgressBar
from qgis.PyQt.QtCore import *
from qgis._core import QgsFields, QgsVectorFileWriter, QgsCoordinateReferenceSystem, QgsCoordinateTransformContext, \
    QgsVectorLayer
from qgis.core import Qgis
from qgis.utils import iface
import qgis


def create_MessageBar(self, message):
    self.start = time()
    self.label = QLabel()
    self.label.setText('                                     ')
    self.progressBar = QProgressBar()
    self.progressMessageBar = iface.messageBar().createMessage(message)
    self.progressMessageBar.layout().addWidget(self.label)
    self.progressMessageBar.layout().addWidget(self.progressBar)
    self.progressBar.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
    iface.messageBar().pushWidget(self.progressMessageBar, Qgis.Info)

def set_MessageBarProgress(self, progress_value, text=''):
    t = time() - self.start
    hms = strftime("%H:%M:%S", gmtime(t))
    self.progressBar.setValue(progress_value)
    self.progressBar.setFormat(f"{hms} - %p%")
    self.label.setText(text)


def move_to_group(self, thing, group, pos=0, expanded=False) -> tuple:
    """
    Move a layer tree node into a layer tree group.
    Moving destroys the original thing and creates a copy. It is the
    copy which is returned.

    Taken from: https://gis.stackexchange.com/a/415161
    Thanks Lorem-Ipsum https://gis.stackexchange.com/users/190371/lorem-ipsum

    :param thing: Thing to move.  Can be a tree node (i.e. a layer or a group) or a map layer, the object or the string name/id.
    :type thing: group name (str), layer id (str), qgis.core.QgsMapLayer, qgis.core.QgsLayerTreeNode

    :param group: Group to move the thing to. If group does not already exist, it will be created.
    :type group: group name (str) or qgis.core.QgsLayerTreeGroup

    :param pos: Position to insert into group. Default is 0.
    :type pos: int
    :param expanded: Collapse or expand the thing moved. Default is False.
    :type expanded: bool

    :return: the moved thing and the group moved to.
    :rtype: tuple
    """


    # thing
    if isinstance(thing, str):
        try:  # group name
            node_object = self.tree_layer.findGroup(thing)
        except:  # layer id
            node_object = self.tree_layer.findLayer(thing)
    elif isinstance(thing, qgis.core.QgsMapLayer):
        node_object = self.tree_layer.findLayer(thing)
    elif isinstance(thing, qgis.core.QgsLayerTreeNode):
        node_object = thing  # tree layer or group


    # group
    if isinstance(group, qgis.core.QgsLayerTreeGroup):
        group_name = group.name()
    else:  # group is str
        group_name = group

    group_object = self.tree_layer.findGroup(group_name)

    if not group_object:
        group_object = self.tree_layer.addGroup(group_name)

    # do the move
    node_object_clone = node_object.clone()
    node_object_clone.setExpanded(expanded)
    group_object.insertChildNode(pos, node_object_clone)

    parent = node_object.parent()
    parent.removeChildNode(node_object)

    return node_object_clone, group_object


def clean_up_gpkg_tmp_files(path):
    for dirpath, dirnames, filenames in os.walk(path):
        for filename in filenames:
            if filename.endswith('.gpkg-shm') or filename.endswith('.gpkg-wal'):
                filepath = os.path.join(dirpath, filename)
                os.remove(filepath)


def create_empty_gpkg_layer(gpkg_path: str, layer_name: str, geometry: int,
                            crs: str, schema: QgsFields, append: bool = False
                            ) -> None:
    """
    Create a empty layer into a gpkg file. The gpkg is created if needed, and can be overwritten if it already exists
    Taken from :
    https://gis.stackexchange.com/questions/417916/creating-empty-layers-in-a-geopackage-using-pyqgis
    Thanks to Germán Carrillo https://gis.stackexchange.com/users/4972/germ%c3%a1n-carrillo

    :param gpkg_path: geopackage file
    :type gpkg_path: str

    :param layer_name: layer to be created
    :type layer_name: str

    :param geometry: Geometry Type. Can be none.
    :type geometry: QgsWkbType

    :param crs: CRS of the geometry. Can be empty
    :type crs: str

    :param schema: Attribute table structure
    :type schema: QgsFields()

    :param append: What to do when gpkg file exists (create or overwrite layer)
    :type append: bool

    :return: None
    :rtype: None
    """
    os.makedirs((os.path.dirname(gpkg_path)), exist_ok=True)
    options = QgsVectorFileWriter.SaveVectorOptions()
    options.driverName = "GPKG"
    options.layerName = layer_name
    if append:
        options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteLayer

    writer = QgsVectorFileWriter.create(
        gpkg_path,
        schema,
        geometry,
        QgsCoordinateReferenceSystem(crs),
        QgsCoordinateTransformContext(),
        options)
    del writer


def create_layer_groups_and_return_number_of_params_to_compute(self):
    if not self.layer_tree_build :
        self.project.writeEntryBool("self.token_config", "Layer_tree_build", True)
        # Generates groups for showing in qgis layer panels
        #tree = qgis.core.QgsProject.instance().layerTreeRoot()
        stage_list = ['2_cleaned_data', '3_imovel_process', '4_merged_AI_and_RL_layers_and_draw_lines']
        subprocess_list = [
            ['01_geom_fixed_fields_cleaned', '02_deduplicate_geom', '03_projected_add_geom_info',
             '04_single_part_orginal_geom_type', '05_original_part_type_to_single_part_point_with_area', '06_data_merged_by_state'],
            ['07_select_imovel_contained_in', '08_select_overlap', '09_geom_fix_imovels', '10_imovels_rl_count',
             '11_add_rl_nb_parts', '12_AI_nb_IDF', '13_AI_classif', '14_merge_params_for_idf_attribution',
             '15_AI_with_idf_attribution'],
            []
        ]
        state_list = []
        municipio_list = []
        # Generate groups and add them to dictionnary
        for working_dir in sorted(glob.glob(f"{self.root_path}/1_raw_data/*/")):
            state = os.path.basename(working_dir[:-1])
            state_list.append(state)
            for sub_dir in sorted(glob.glob(working_dir + "/*/")):
                # Iterate on municipios
                municipio = os.path.basename(sub_dir[:-1])
                municipio_list.append(municipio)
        for state in state_list:
            #todo rajouter des try excep pour éviter groups en double :)
            try:
                group_object_state = self.dict_group_layers[state]
            except:
                group_object_state = self.tree_layer.addGroup(state)
                self.dict_group_layers[state] = group_object_state

            # spatial case for 1_row_data that do noot have subdirectory before municipio directory
            group_object_stage = self.tree_layer.addGroup('1_raw_data')
            group_object_stage, parent = move_to_group(self, group_object_stage, group_object_state)
            self.dict_group_layers[f"{state}_1_raw_data"] = [group_object_stage, parent]
            for municipio in municipio_list:
                group_object_municipio = self.tree_layer.addGroup(municipio)
                group_object_municipio, parent = move_to_group(self, group_object_municipio, group_object_stage)
                self.dict_group_layers[f"{state}_1_raw_data_{municipio}"] = [group_object_municipio, parent]

            # now proceed with computed_data groups
            i = 0
            for stage in stage_list:
                group_object_stage = self.tree_layer.addGroup(stage)
                group_object_stage, parent = move_to_group(self, group_object_stage, group_object_state)
                self.dict_group_layers[f"{state}_{stage}"] = [group_object_stage, parent]
                for subprocess in subprocess_list[i]:
                    group_object_subprocess = self.tree_layer.addGroup(subprocess)
                    group_object_subprocess, parent = move_to_group(self, group_object_subprocess, group_object_stage)
                    self.dict_group_layers[f"{state}_{stage}_{subprocess}"] = [group_object_subprocess, parent]
                    if subprocess != "06_data_merged_by_state":
                        for municipio in municipio_list:
                            group_object_municipio = self.tree_layer.addGroup(municipio)
                            group_object_municipio, parent = move_to_group(self, group_object_municipio, group_object_subprocess)

                            self.dict_group_layers[f"{state}_{stage}_{subprocess}_{municipio}"] = [group_object_municipio,
                                                                                                   parent]
                i +=1
    # count valid shapefile for progressbar
    j = 0
    for working_dir in sorted(glob.glob(f"{self.root_path}/1_raw_data/*/")):
        for sub_dir in sorted(glob.glob(working_dir + "/*/")):
            for sub_sub_dir in sorted(glob.glob(sub_dir + "/*/")):
                param = os.path.basename(sub_sub_dir[:-1])
                shp_src = (sub_sub_dir + param + ".shp")
                shp_src_layer = QgsVectorLayer(shp_src)
                if shp_src_layer.isValid() and shp_src_layer.featureCount() >= 1:
                    if shp_src_layer.featureCount() >= 1:
                        j += 1

    return j


def print_progress_bar_info(i, i_tot, message, municipio, param, state):
    percent = int((i / i_tot) * 100)
    iface.statusBarIface().showMessage(
        f"Processing {state} {municipio} {param} : {i}/{i_tot} ({percent} %) - {message} ")