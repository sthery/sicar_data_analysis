#!/usr/bin/env python3
# coding: utf-8
"""
Coding:
    Sylvain Théry - UMR 5281 ART-Dev - 2022/2023
    CC-by-SA-NC V4
Algorithm:
    Pierre Gautereau - UMR 8586 Prodig - Paris, France
    Sylvain Théry - UMR 5281 ART-Dev - Montpellier, France
    Ludivine Eloy - UMR 5281 ART-Dev - Montpellier, France
    Eve-Anne Bühler - Núcleo de Estudos Geoambientais (NUCLAMB) - Rio de Janeiro, Brazil
"""

import glob
import os
import shutil
import time

from PyQt5.QtCore import QVariant
from qgis.core import QgsMessageLog, Qgis, QgsProject, \
    QgsCoordinateReferenceSystem, QgsField, QgsVectorLayer

# Append the path where processing plugin can be found
import processing
from qgis.utils import iface

from .Z_miscellaneous import create_MessageBar, set_MessageBarProgress, \
    create_layer_groups_and_return_number_of_params_to_compute


def A2_preprocessing_data(self):
    # remove directory if exists
    try:
        shutil.rmtree(f"{self.root_path}/2_cleaned_data/")
    except:
        pass

    # set dest path and create directory if not exists
    dest_path = f"{self.root_path}/2_cleaned_data/"
    os.makedirs(dest_path, exist_ok=True)

    # create layer groups in qgis layer panel
    number_param_layer = create_layer_groups_and_return_number_of_params_to_compute(self)

    # let's start preprocessing
    # iterate an states
    message = self.tr("Preprocessing data by params and municipio...")
    create_MessageBar(self, message)
    self.progressBar.setMaximum(number_param_layer)
    iface.messageBar().pushWidget(self.progressMessageBar, Qgis.Info)

    j=0 # counter for iface.messageBar()
    for working_dir in sorted(glob.glob(f"{self.root_path}/1_raw_data/*/")):
        state = os.path.basename(working_dir[:-1])
        list_municipios =[]
        for sub_dir in sorted(glob.glob(working_dir + "/*/")):
            # Iterate on municipios
            municipio = os.path.basename(sub_dir[:-1])
            list_municipios.append(municipio)

            # iterate on params name
            for sub_sub_dir in sorted(glob.glob(sub_dir + "/*/")):
                # iterate on params name
                param = os.path.basename(sub_sub_dir[:-1])

                # list_params=sorted(set(list_params()))

                shp_src = (sub_sub_dir + param + ".shp")

                shp_src_layer = QgsVectorLayer(shp_src, f"{municipio}_{param}")


                if shp_src_layer.isValid():
                    if shp_src_layer.featureCount() >= 1:
                        # add layer to 1_raw_data_group
                        QgsProject.instance().addMapLayer(shp_src_layer, False)
                        dest_group = self.dict_group_layers[f"{state}_1_raw_data_{municipio}"][1]
                        dest_group.findGroup(municipio).addLayer(shp_src_layer)
                        i = 0 # couter for status bar message

                        set_MessageBarProgress(self, j, municipio)
                        j += 1
                        i_tot = 5
                        # fix geometries
                        i+=1
                        message = 'fix geometries'
                        percent = int((i / i_tot) * 100)
                        iface.statusBarIface().clearMessage()
                        iface.statusBarIface().showMessage(
                            f"Processing {state} {municipio} {param} : {i}/{i_tot} ({percent} %) - {message} ")

                        mem_layer_geom_fix = A2_1_remove_add_fields_and_fix_geom(self, municipio, param,
                                                                                 shp_src_layer, state, dest_path)

                        # deduplicate
                        i += 1
                        message = 'deduplicate geom'
                        percent = int((i / i_tot) * 100)
                        iface.statusBarIface().clearMessage()

                        iface.statusBarIface().showMessage(
                            f"Processing {state} {municipio} {param} : {i}/{i_tot} ({percent} %) - {message} ")


                        mem_layer_deduplicate = A2_2_deduplicate_geom(self, mem_layer_geom_fix,
                                                                      municipio, param, state, dest_path)


                        # reproject and add area and number parts
                        i += 1
                        message= "reproject and add area and number parts"
                        percent = int((i / i_tot) * 100)
                        iface.statusBarIface().clearMessage()

                        iface.statusBarIface().showMessage(
                            f"Processing {state} {municipio} {param} : {i}/{i_tot} ({percent} %) - {message} ")

                        mem_layer_reproject_with_area_info = A2_3_project_and_add_geom_cols_and_count_parts(self,
                                                                                                            mem_layer_deduplicate, municipio, param, state, dest_path)

                        # split polyg geom into single part and calculate new area
                        i += 1
                        message = "split original geom into single part and calculate new area"
                        percent = int((i / i_tot) * 100)
                        iface.statusBarIface().clearMessage()
                        time.sleep(0.1)
                        iface.statusBarIface().showMessage(
                            f"Processing {state} {municipio} {param} : {i}/{i_tot} ({percent} %) - {message} ")

                        mem_layer_original_geom_type_single_part_with_area = \
                            A2_4_original_geom_type_to_single_part_with_area(self,mem_layer_reproject_with_area_info,
                                                                            municipio, param, state, dest_path)

                        # split polyg geom into single part point with new area
                        i += 1
                        message ="convert original geom into single part points"
                        percent = int((i / i_tot) * 100)
                        iface.statusBarIface().clearMessage()
                        time.sleep(0.1)
                        iface.statusBarIface().showMessage(
                            f"Processing {state} {municipio} {param} : {i}/{i_tot} ({percent} %) - {message} ")

                        mem_layer_original_geom_type_to_point_with_area = \
                            A2_5_original_geom_type_to_single_part_point_with_area(self,
                                 mem_layer_reproject_with_area_info,
                                 municipio, param, state,
                                 dest_path)

                        # merging data by state
                        A2_6_merge_params_by_state(self, dest_path)
                    else:
                        message= self.tr(f"Empty shapefile for {state} {municipio} {param}")
                        QgsMessageLog.logMessage(self.tr(message), self.plugin_name, level=Qgis.Warning)
                else:
                    message = self.tr(f"Invalid shapefile for {state} {municipio} {param}")
                    QgsMessageLog.logMessage(self.tr(message), self.plugin_name, level=Qgis.Critical)

        #TODO Concaténer les fichiers par param pour les géométries singles et multiples, en point comme en geom_originale
    iface.statusBarIface().clearMessage()
    iface.messageBar().clearWidgets()


def A2_1_remove_add_fields_and_fix_geom(self, municipio, param, shp_src_layer, state, dest_path):
    fieldnames_list_to_delete = []
    for field in shp_src_layer.fields():
        if field.name() != 'IDF' and field.name() != 'COD_IMOVEL':
            fieldnames_list_to_delete.append(field.name())
    param_algo = {'INPUT': shp_src_layer,
                  'COLUMN': fieldnames_list_to_delete,
                  'OUTPUT': 'TEMPORARY_OUTPUT'}
    result_del_fields = processing.run('qgis:deletecolumn', param_algo)
    mem_layer_remove_field = result_del_fields['OUTPUT']


    # add fields to keep track of origin
    # (special name 4 Area Imovel because of the final jointure)
    if param == "AREA_IMOVEL":
        list_fields = ['state_AI', 'munici_AI', 'param_AI']
    elif param == "RESERVA_LEGAL":
        list_fields = ['state_AI', 'munici_RL', 'param_RL']
    else:
        list_fields = ['state', 'municipio', 'param']

    list_values = [state, municipio, param]

    result_layer_provider = mem_layer_remove_field.dataProvider()
    mem_layer_remove_field.startEditing()
    for field in list_fields:
        result_layer_provider.addAttributes([QgsField(field, QVariant.String)])
    mem_layer_remove_field.updateFields()

    mem_layer_remove_field.startEditing()
    for feature in mem_layer_remove_field.getFeatures():
        for i in range(3):
            feature[list_fields[i]] = list_values[i]
        mem_layer_remove_field.updateFeature(feature)
    mem_layer_remove_field.commitChanges()

    # fix geometries
    result_fixgeom = processing.run("native:fixgeometries", {
        'INPUT': mem_layer_remove_field,
        'OUTPUT': 'TEMPORARY_OUTPUT', 'METHOD': 0})

    mem_layer_geom_fix = result_fixgeom['OUTPUT']

    # define gpkg dest name and create dest path if not exists
    subprocess='01_geom_fixed_fields_cleaned'
    layer = f'{dest_path}/{subprocess}/{state}/{param}_{municipio}.gpkg'
    os.makedirs(os.path.dirname(layer), exist_ok=True)
    processing.run("native:savefeatures", {
        'INPUT': mem_layer_geom_fix,
        'OUTPUT': layer, 'LAYER_NAME': '', 'DATASOURCE_OPTIONS': '',
        'LAYER_OPTIONS': "-a_srs EPSG:4674"})
    layer = QgsVectorLayer(layer, f"{municipio}_{param}")
    QgsProject.instance().addMapLayer(layer, False)
    dest_group = self.dict_group_layers[f"{state}_2_cleaned_data_{subprocess}_{municipio}"][1]
    dest_group.findGroup(municipio).addLayer(layer)


    return mem_layer_geom_fix


def A2_2_deduplicate_geom(self, gpkg_single_part_name, municipio, param, state, dest_path):
    # geometrically deduplicate features of the layer

    result_deduplicate = processing.run("native:deleteduplicategeometries", {
        'INPUT': gpkg_single_part_name,
        'OUTPUT': "TEMPORARY_OUTPUT"})

    subprocess= '02_deduplicate_geom'
    layer = f'{dest_path}/{subprocess}/{state}/{param}_{municipio}.gpkg'
    os.makedirs(os.path.dirname(layer), exist_ok=True)
    processing.run("native:savefeatures", {
        'INPUT': result_deduplicate['OUTPUT'],
        'OUTPUT': layer, 'LAYER_NAME': '', 'DATASOURCE_OPTIONS': '',
        'LAYER_OPTIONS': "-a_srs EPSG:4674"})
    layer = QgsVectorLayer(layer, f"{municipio}_{param}")
    QgsProject.instance().addMapLayer(layer, False)
    dest_group = self.dict_group_layers[f"{state}_2_cleaned_data_{subprocess}_{municipio}"][1]
    dest_group.findGroup(municipio).addLayer(layer)
    return result_deduplicate['OUTPUT']


def A2_3_project_and_add_geom_cols_and_count_parts(self, mem_layer_deduplicate, municipio, param, state, dest_path):
    # reproject
    params = {
        'INPUT': mem_layer_deduplicate,
        'OPERATION': '+proj=pipeline +step +proj=unitconvert +xy_in=deg +xy_out=rad +step +proj=push +v_3 +step +proj=cart +ellps=WGS84 +step +proj=helmert +x=67.35 +y=-3.88 +z=38.22 +step +inv +proj=cart +ellps=aust_SA +step +proj=pop +v_3 +step +proj=poly +lat_0=0 +lon_0=-54 +x_0=5000000 +y_0=10000000 +ellps=aust_SA',
        'OUTPUT': 'TEMPORARY_OUTPUT', 'TARGET_CRS': QgsCoordinateReferenceSystem('EPSG:29101')
    }
    result_reproj = processing.run("native:reprojectlayer", params)
    mem_layer_projected = result_reproj['OUTPUT']

    # Due to invalid géométries observed after deduplicate, with need ton fix geom once again before reprojecting :/
    # fix geometries
    result_fixgeom = processing.run("native:fixgeometries", {
        'INPUT': mem_layer_projected,
        'OUTPUT': 'TEMPORARY_OUTPUT', 'METHOD': 0})

    mem_layer_projected_fix = result_fixgeom['OUTPUT']

    # Add geometry column
    params = {
        'CALC_METHOD': 0,
        'INPUT': mem_layer_projected_fix,
        'OUTPUT': 'TEMPORARY_OUTPUT'}
    result_measure = processing.run("qgis:exportaddgeometrycolumns", params)

    # Rename area column if exist to precise it concerns multipart feature
    try:  # beacuse there's no area field for point layer
        # define diferrent field name for AI_SP_area and IR_SP_AREA
        if param == "AREA_IMOVEL":
            field_name = 'area_Multipart_AI'
        elif param == "RESERVA_LEGAL":
            field_name = 'area_Multipart_RL'
        else:
            field_name = 'area_Multipart'

        # convert area to heactares with two decimal
        params =  {
            'INPUT': result_measure['OUTPUT'],
            'FIELD_NAME': field_name,
            'FIELD_TYPE': 0, 'FIELD_LENGTH': 0, 'FIELD_PRECISION': 0,
            'FORMULA': 'to_int("area" /100)/100', 'OUTPUT': 'TEMPORARY_OUTPUT'}
        result_area_ha = processing.run("native:fieldcalculator", params)

        # remove perimeter field and area
        param_algo = {'INPUT': result_area_ha['OUTPUT'],
                      'COLUMN': ['perimeter', 'area'],
                      'OUTPUT': 'TEMPORARY_OUTPUT'}
        result_del_field = processing.run('qgis:deletecolumn', param_algo)

        mem_layer_new_area = result_del_field['OUTPUT']

    except:  # turn around to use the same variable name anyway for points layer
       mem_layer_new_area = result_measure['OUTPUT']

    # add number of parts for multiparts layers
    if param == "AREA_IMOVEL":
        field_name = 'nb_parts_AI'
    elif param == "RESERVA_LEGAL":
        field_name = 'nb_parts_RL'
    else:
        field_name = 'nb_parts'
    params = {
        'INPUT': mem_layer_new_area,
        'FIELD_NAME': field_name, 'FIELD_TYPE': 1,
        'FIELD_LENGTH': 0,
        'FIELD_PRECISION': 0,
        'FORMULA': 'num_geometries($geometry)',
        'OUTPUT': 'TEMPORARY_OUTPUT'}
    result_add_nb_parts = processing.run("native:fieldcalculator", params)
    mem_layer_geom_proj_nb_parts = result_add_nb_parts['OUTPUT']

    # Finally write gpkg to disk
    subprocess='03_projected_add_geom_info'
    layer = f'{dest_path}/{subprocess}/{state}/{param}_{municipio}.gpkg'
    os.makedirs(os.path.dirname(layer), exist_ok=True)
    processing.run("native:savefeatures", {
        'INPUT': mem_layer_geom_proj_nb_parts,
        'OUTPUT': layer, 'LAYER_NAME': '', 'DATASOURCE_OPTIONS': '',
        'LAYER_OPTIONS': "-a_srs 'epsg:29101'"})
    layer = QgsVectorLayer(layer, f"{municipio}_{param}")
    QgsProject.instance().addMapLayer(layer, False)
    dest_group = self.dict_group_layers[f"{state}_2_cleaned_data_{subprocess}_{municipio}"][1]
    dest_group.findGroup(municipio).addLayer(layer)
    return mem_layer_geom_proj_nb_parts


def A2_4_original_geom_type_to_single_part_with_area(self, mem_layer_reproject_with_geom_info, municipio, param, state, dest_path):
    # convert multipart so single
    result_multipart = processing.run("native:multiparttosingleparts", {'INPUT': mem_layer_reproject_with_geom_info,
                                                     'OUTPUT': 'TEMPORARY_OUTPUT'})

    mem_layer_single_part= result_multipart['OUTPUT']

    # Add geometry column
    params = {
        'CALC_METHOD': 0,
        'INPUT': mem_layer_single_part,
        'OUTPUT': 'TEMPORARY_OUTPUT'}
    result_measure = processing.run("qgis:exportaddgeometrycolumns", params)
    result_measure['OUTPUT']

    try:  # beacuse there's no area field for point layer
        # define diferrent field name for AI_SP_area and IR_SP_AREA
        if param == "AREA_IMOVEL":
            field_name = 'area_SinglePart_AI'
        elif param == "RESERVA_LEGAL":
            field_name = 'area_SinglePart_RL'
        else:
            field_name = 'area_SinglePart'

        # convert area to heactares
        params_ha = {
            'INPUT': result_measure['OUTPUT'],
            'FIELD_NAME': field_name,
            'FIELD_TYPE': 0, 'FIELD_LENGTH': 0, 'FIELD_PRECISION': 0,
            'FORMULA': 'to_int("area" /100)/100', 'OUTPUT': 'TEMPORARY_OUTPUT'}
        result_area_ha = processing.run("native:fieldcalculator", params_ha)

        # remove perimeter field
        param_algo = {'INPUT': result_area_ha['OUTPUT'],
                      'COLUMN': ['perimeter', 'area'],
                      'OUTPUT': 'TEMPORARY_OUTPUT'}
        result_del_field = processing.run('qgis:deletecolumn', param_algo)

        mem_layer_single_part_with_area = result_del_field['OUTPUT']
    except:  # turn around to use the same variable name anyway for points layer
        mem_layer_single_part_with_area = result_measure['OUTPUT']
    subprocess='04_single_part_orginal_geom_type'
    layer = f'{dest_path}/04_single_part_orginal_geom_type/{state}/{param}_{municipio}.gpkg'
    os.makedirs(os.path.dirname(layer), exist_ok=True)
    processing.run("native:savefeatures", {
        'INPUT': mem_layer_single_part_with_area,
        'OUTPUT': layer, 'LAYER_NAME': '', 'DATASOURCE_OPTIONS': '',
        'LAYER_OPTIONS': "-a_srs 'epsg:29101'"})
    layer = QgsVectorLayer(layer, f"{municipio}_{param}")
    QgsProject.instance().addMapLayer(layer, False)
    dest_group = self.dict_group_layers[f"{state}_2_cleaned_data_{subprocess}_{municipio}"][1]
    dest_group.findGroup(municipio).addLayer(layer)
    return mem_layer_single_part_with_area


def A2_5_original_geom_type_to_single_part_point_with_area(self, mem_layer_reproject_with_area_info, municipio, param, state, dest_path):
    params_point={
        'INPUT': mem_layer_reproject_with_area_info,
        'ALL_PARTS': True, 'OUTPUT': 'TEMPORARY_OUTPUT'}
    result_geom_to_point = processing.run("native:pointonsurface", params_point)

    # Finally write gpkg to disk
    subprocess ='05_original_part_type_to_single_part_point_with_area'
    layer = f'{dest_path}/05_original_part_type_to_single_part_point_with_area/{state}/{param}_{municipio}.gpkg'
    os.makedirs(os.path.dirname(layer), exist_ok=True)

    processing.run("native:savefeatures", {
        'INPUT': result_geom_to_point['OUTPUT'],
        'OUTPUT': layer,
        'LAYER_NAME': '', 'DATASOURCE_OPTIONS': '',
        'LAYER_OPTIONS': "-a_srs 'epsg:29101'"})
    layer = QgsVectorLayer(layer, f"{municipio}_{param}")
    QgsProject.instance().addMapLayer(layer, False)
    dest_group = self.dict_group_layers[f"{state}_2_cleaned_data_{subprocess}_{municipio}"][1]
    dest_group.findGroup(municipio).addLayer(layer)
    return result_geom_to_point['OUTPUT']


def A2_6_merge_params_by_state(self, dest_path):
    """

    :return:
    """
    params = sorted(list_params(f"{self.root_path}/1_raw_data/"))

    for directory in glob.glob(f'{dest_path}/00_geom_fixed_fields_cleaned/*/'):
        state = os.path.basename(directory[:-1])
        os.makedirs(os.path.dirname(f'{dest_path}/06_data_merged_by_state/'), exist_ok=True)
        for param in params:

            if len(glob.glob(directory + f"{param}*.gpkg")) > 0:
                # try:
                print('ok')
                processing.run("native:mergevectorlayers", {
                    'LAYERS': glob.glob(directory + f"/{param}*.gpkg"),
                    'OUTPUT': f'ogr:dbname=\'{dest_path}/06_data_merged_by_state/{state}.gpkg\' table="{param}" (geom)'})



def list_params(path):

    params_list = []
    for dir in glob.glob(f"{path}/*/"):
        for sub_dir in glob.glob(dir + "/*/"):
            # itération sur les municipios

            for sub_sub_dir in glob.glob(sub_dir + "/*/"):
                params_list.append(os.path.basename(sub_sub_dir[:-1]))
            params_list = list(set(params_list))
    return params_list

    # def A2_1_merge_params_by_state():
    #     """
    #
    #     :return:
    #     """
    #     params = sorted(list_params(f"{root_path}/1_raw_data/"))
    #
    #     for directory in glob.glob(f'{dest_path}/00_geom_fixed/*/'):
    #         state = os.path.basename(directory[:-1])
    #         os.makedirs(os.path.dirname(f'{dest_path}/06_data_merged_by_state/'), exist_ok=True)
    #         for param in params:
    #             if param == 'AREA_IMOVEL' or param == 'RESERVA_LEGAL':
    #                 # print(param)
    #                 # print(glob.glob(directory + f"/{param}*.gpkg"))
    #                 if len(glob.glob(directory + f"{param}*.gpkg")) > 0:
    #                     # try:
    #                     print('ok')
    #                     processing.run("native:mergevectorlayers", {
    #                         'LAYERS': glob.glob(directory + f"/{param}*.gpkg"),
    #                         'OUTPUT': f'ogr:dbname=\'{dest_path}/06_data_merged_by_state/{state}.gpkg\' table="{param}" (geom)'})
    #                     # except:
    #                     #     print(f'Issue when trying to concatenate  {param} files for {state}...')
    #                     #     logging.error(f'Issue when trying to concatenate  {param} files for {state}...')



