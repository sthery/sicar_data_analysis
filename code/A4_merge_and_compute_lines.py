#!/usr/bin/env python3
# coding: utf-8
"""
Coding:
    Sylvain Théry - UMR 5281 ART-Dev - 2022/2023
    CC-by-SA-NC V4
Algorithm:
    Pierre Gautereau - UMR 8586 Prodig - Paris, France
    Sylvain Théry - UMR 5281 ART-Dev - Montpellier, France
    Ludivine Eloy - UMR 5281 ART-Dev - Montpellier, France
    Eve-Anne Bühler - Núcleo de Estudos Geoambientais (NUCLAMB) - Rio de Janeiro, Brazil
"""

import glob
import logging
import os

from datetime import datetime
import sys

from qgis.core import QgsApplication

import processing
from processing.core.Processing import Processing

Processing.initialize()


def A4_1_merge_RL(self, dest_path):
    """
    Merge Reserva legal layer of muncipios by state
    :param self:
    :type self:
    :param dest_path:
    :type dest_path:
    :return:
    :rtype:
    """
    source_path_list = [f"{self.root_path}/2_cleaned_data/03_projected_add_geom_info/", f"{self.root_path}/2_cleaned_data/05_original_part_type_to_single_part_point_with_area/"]
    filename_suffix=['polygon', 'point']
    i=0
    i_tot=0
    for source_path in source_path_list:
        for working_dir in sorted(glob.glob(source_path + "/*/")):
            state = os.path.basename(working_dir[:-1])
            i_tot+= len(glob.glob(working_dir + "/*.gpkg"))
    # TODO put a messagebar
    for source_path in source_path_list:
        RL_file_list = []
        for working_dir in sorted(glob.glob(source_path + "/*/")):
            state = os.path.basename(working_dir[:-1])
            for filename in sorted(glob.glob(working_dir + "/*.gpkg")):
                left, right = filename.split('.')[0].rsplit("_", 1)
                if self.verbose:
                    print(f" filename {filename}")
                    print(left)
                if os.path.basename(left) == "RESERVA_LEGAL":
                    if self.verbose:
                        print(filename)
                    RL_file_list.append(f"{filename}|layername={os.path.basename(filename)[:-5]}")
                dest_file = f"{dest_path}/{state}/RESERVA_LEGAL_merged_{filename_suffix[i]}.gpkg"

            os.makedirs(os.path.dirname(dest_file), exist_ok=True)
            params = {'LAYERS': RL_file_list,
                      'CRS': None,
                      'OUTPUT': dest_file}
            RL_merged = processing.run("native:mergevectorlayers", params)
    if self.verbose:
        print(f"dest_file : {dest_file} {type(dest_file)}")
    return dest_file





def A4_2_merge_AI(self, dest_path):
    AI_file_list = []
    print(f'Merging AI')
    for working_dir in sorted(glob.glob(f"{self.root_path}/3_imovel_process/15_AI_with_idf_attribution/*/")):
        state = os.path.basename(working_dir[:-1])
        for filename in sorted(glob.glob(working_dir + "/*.gpkg")):
            AI_file_list.append(f"{filename}")
        dest_file = f"{dest_path}/{state}/AREA_IMOVEL_merged.gpkg"
        os.makedirs(os.path.dirname(dest_file), exist_ok=True)
        params = {'LAYERS': AI_file_list,
                  'CRS': None,
                  'OUTPUT': dest_file}
        AI_merged = processing.run("native:mergevectorlayers", params)

    return dest_file, state, dest_path

def A3_3_generate_hublines(self, AI_file, RL_file, state, dest_path):
    if self.verbose:
        print(f"RL FILE {RL_file} {type(RL_file)}")
        print(f"AI FILE {AI_file} {type(AI_file)}")

    dest_file = f"{dest_path}/{state}/HUB_LINES.gpkg"
    processing.run("native:hublines", {
        'HUBS': RL_file,
        'HUB_FIELD': 'IDF', 'HUB_FIELDS': ['IDF', 'state', 'municipio'],
        'SPOKES': AI_file,
        'SPOKE_FIELD': 'IDF', 'SPOKE_FIELDS': ['state_AI', 'munici_AI', 'param_AI', 'IDF', 'IDF_from'],
        'GEODESIC': False, 'GEODESIC_DISTANCE': 1000, 'ANTIMERIDIAN_SPLIT': False, 'OUTPUT': dest_file})

def merge_and_draw_lines(self):
    dest_path = f"{self.root_path}/4_merged_AI_and_RL_layers_and_draw_lines/"
    os.makedirs(dest_path, exist_ok=True)
    RL_file = A4_1_merge_RL(self, dest_path)

    AI_file, state, dest_path = A4_2_merge_AI(self, dest_path)

    A3_3_generate_hublines(self, AI_file, RL_file, state, dest_path)

