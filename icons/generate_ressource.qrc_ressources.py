#!/bin/bash
echo "<!DOCTYPE RCC>" >../resources.qrc
echo "<RCC>" >>../resources.qrc
echo "    <qresource prefix='/plugins/sicar_data_analysis' >" >>../resources.qrc
for icon in *.png
 do
   echo "       <file>./icons/$icon</file>" >>../resources.qrc
done
echo "    </qresource>" >>../resources.qrc
echo "</RCC>" >>../resources.qrc
cd ..
pyrcc5 resources.qrc -o resources.py
